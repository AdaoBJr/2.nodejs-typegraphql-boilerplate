<img src="readme/nodejs_animation.gif" width="50" height="50" >

<h1 align="center"> NodeJs - TypeGraphQL - BoilerPlate </h1>

## 🧑🏻‍💻🧑🏻‍💻Desenvolvido por

@[AdaoBJr](https://bitbucket.org/AdaoBJr/)
<br>

---

## 💡 Sobre o Projeto

O Projeto NodeJs-TypeGraphQL-BoilerPlate é um BoilerPlate desenvolvido para uso com NodeJs - ApolloServer (TypeGraphQL) - TypeScript.

## 🛠 Tecnologias Usadas

- NodeJS
- TypeScript
- Express
- Apollo-Server
- GraphQL
- TypeGraphQL
- Babel
- GraphQL-Tools

## 🧙‍♂️ Como Iniciar o Projeto

Primeiro faça a clonagem do projeto em algum diretorio do seu computador:

```bash
> cd ~
> cd Documents/Mcx
> git clone git@bitbucket.org:AdaoBJr/2.nodejs-typegraphql-boilerplate.git
> cd 2.nodejs-typegraphql-boilerplate
```

Depois disso instale as dependências:

```bash
> yarn ou npm install
```

Por fim, rode o seguinte script para iniciar o projeto:

```bash
> yarn start
```

O projeto vai iniciar em http://localhost:3000/graphql.
