import { Mutation, Query, Resolver } from 'type-graphql';

@Resolver()
class users {
  @Query(() => String)
  async users() {
    return 'users data...';
  }

  @Mutation(() => String)
  async createUser() {
    return 'User criado com sucesso!!!';
  }
}

export default { users };
